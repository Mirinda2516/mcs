#include<stdio.h>
#include<omp.h>
#include<stdlib.h>
#include<time.h>


void process(int a, int b, int x, float p, int* isInB, int* numSteps, unsigned int* seed)
{   
    int steps = 0;
    int current = x;
    while(current!=a && current!= b) {
        float val = (float)(rand_r(seed) % RAND_MAX) / RAND_MAX;
        if(val < p) {
            current += 1;
        } else {
            current -= 1;
        }
        steps += 1;
    } 
    
    if(current == b){
        *isInB = 1;
    } else {
        *isInB = 0;
    }
    *numSteps = steps;
}

int main(int argc,char* argv[])
{
    int a = atoi(argv[1]);
    int b = atoi(argv[2]);
    int x = atoi(argv[3]);
    int N = atoi(argv[4]);
    float p = atof(argv[5]);
    int P = atoi(argv[6]);
    srand((unsigned int)time(NULL));    
    int numEndedInB = 0;
    int totalSteps = 0;
    omp_set_num_threads(P);
    double start = omp_get_wtime();
    #pragma omp parallel shared(a,b,x,N,p,P,numEndedInB,totalSteps)
    {
	unsigned int seed;
	#pragma omp critical
	{	
	   seed = rand();
	}
        #pragma omp for schedule(auto) reduction(+:numEndedInB,totalSteps)
        for(int i = 0;i < N; i++) {
           int steps = 0;
           int isInB = 0;
           process(a,b,x,p,&isInB,&steps, &seed);
           if(isInB == 1) {
                numEndedInB += 1;
           }  
           totalSteps += steps;
           
        }
    }
    double time  = omp_get_wtime() - start;
    freopen("stats.txt","wt",stdout);
    printf("%f %f %fs %d %d %d %d %f %d\n",
    (double)numEndedInB / N,(double)totalSteps / N,
    time,a,b,x,N,p,P);
    return 0;
}










