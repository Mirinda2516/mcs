#include<time.h>
#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<semaphore.h>


int P = 4;
int n = 16;
int init_m = 4;
int* array;
int* buf;

long result = 0;
double elapsed, qsrt_elapsed;
struct timespec begin, end;
struct timespec qsrt_begin, qsrt_end;

void swap(int* a, int* b) {
  int x = *a;
  *a = *b;
  *b = x;
}

void merge(int start, int mid, int end) {
  if (mid == end) {
    return;
  }
  int first_ind = start;
  int sec_ind = mid;
  //printf("%d %d\n", mid, end);
  for (int i = start; i < end; ++i)
    buf[i] = array[i];
  for (int i = start; i < end; ++i) {
    if (buf[first_ind] < buf[sec_ind]) {
      array[i] = buf[first_ind];
      first_ind++;
      if (first_ind == mid) {
        while (sec_ind < end) {
          ++i;
          array[i] = buf[sec_ind];
          sec_ind++;
        }
      }
    } else {
      array[i] = buf[sec_ind];
      sec_ind++;
      if (sec_ind == end) {
        while (first_ind < mid) {
          ++i;
          array[i] = buf[first_ind];
          first_ind++;
        }
      }
    }
  }
}

void merge_sort(int start, int end) {
  if (end - start > 2) {
    int mid = (start + end) / 2;
    merge_sort(start, mid);
    merge_sort(mid, end);
    merge(start, mid, end);
  } else {
    if (end == start + 2) {
      if (array[start] > array[end - 1])
        swap(&array[start], &array[end - 1]);
    } else return;
  }
}

void* thread_func(void* param) {
  int* arg = (int*) param;
  merge_sort(arg[0], arg[1]);
  pthread_exit(NULL);
}

void* thread_func2(void* param) {
  int* arg = (int*) param;
  merge(arg[0], arg[1], arg[2]);
  free(arg);
  pthread_exit(NULL);
}

int cmp(const void* a, const void* b) {
  return *(int*) a - *(int*) b;
}

int main(int argc, char* argv[]) {
  if (argc > 3) {
    n = atoi(argv[1]);
    init_m = atoi(argv[2]);
    P = atoi(argv[3]);
  }
  int m = init_m;
  int rc = 0;
  buf = (int*) malloc(n*sizeof(int));
  array = (int*) malloc(n*sizeof(int));
  int* edges = (int*) malloc((n + 2)*sizeof(int));
  int* qsrt = (int*) malloc(n*sizeof(int));
  for (int i = 0; i < n; ++i) {
    array[i] = rand() % 100;
    qsrt[i] = array[i];
  }
  clock_gettime(CLOCK_REALTIME, &qsrt_begin);
  qsort(qsrt, n, sizeof(int), cmp);
  clock_gettime(CLOCK_REALTIME, &qsrt_end);
  qsrt_elapsed = qsrt_end.tv_sec - qsrt_begin.tv_sec;
  qsrt_elapsed += (qsrt_end.tv_nsec - qsrt_begin.tv_nsec)/1000000000.0;
  free(qsrt);
  FILE* dat = fopen("data.txt", "w");
  for (int i = 0; i < n; ++i) {
      fprintf(dat, "%d ", array[i]);
  }
  fprintf(dat, "\n");
  pthread_t threads[P];
  if (n/m < P) {
    m = n/P + 1;
  }
  clock_gettime(CLOCK_REALTIME, &begin);
  int ended = 0;
  int last_edge = 0;
  int edge_num = 0;
  int last_thread = P;
  edges[last_edge++] = 0;
  while (ended != n) {
    for (int i = 1; i < P; ++i) {
      ended = (ended + m > n) ? n : ended + m;
      edges[last_edge++] = ended;
      rc = pthread_create(&threads[i], NULL, thread_func, &edges[last_edge-2]);
      if (ended == n) {
        edge_num = last_edge;
        last_thread = i+1;
        break;
      }
    }
    ended = (ended + m > n) ? n : ended + m;
    edges[last_edge++] = ended;
    merge_sort(edges[last_edge-2], edges[last_edge - 1]);
    for (int i = 1; i < last_thread; ++i) {
      rc = pthread_join(threads[i], NULL);
    }
    //printf("%d ended\n", ended);
  }
  if (edge_num == 0) {
    edge_num = last_edge;
  }
  int count = 0;
  while (m < n) {
    count++;
    m *= 2;
  }
  printf("first phase\n" );
  int step = 1;
  for (int j = 1; j <= count; ++j) {
    ended = 0;
    last_edge = 0;
    last_thread = P;
    while (ended != n) {
      //printf("%d ended2 %d\n", ended, step);
      for (int i = 1; i < P; ++i) {
        if (last_edge + step >= edge_num) {
          last_thread = i;
          ended = edges[edge_num - 1];

          break;
        }
        int* arg = (int*) malloc(sizeof(int)*3);
        arg[0] = edges[last_edge];
        arg[1] = edges[last_edge + step];
        if (last_edge + step*2 >= edge_num) {
          arg[2] = edges[edge_num - 1];
        } else {
          arg[2] = edges[last_edge + step*2];
        }
        ended = arg[2];
        last_edge += step*2;
        rc = pthread_create(&threads[i], NULL, thread_func2, arg);
      }
      if (last_edge + step < edge_num - 1) {
        int* arg = (int*) malloc(sizeof(int)*3);
        arg[0] = edges[last_edge];
        arg[1] = edges[last_edge + step];
        if (last_edge + step*2 >= edge_num) {
          arg[2] = edges[edge_num - 1];
        } else {
          arg[2] = edges[last_edge + step*2];
        }
        ended = arg[2];
        last_edge += step*2;
        merge(arg[0], arg[1], arg[2]);
        free(arg);
      } else {
        ended = n;
      }
      //printf("%d\n", ended);
      for (int i = 1; i < last_thread; ++i) {
        rc = pthread_join(threads[i], NULL);
      }
    }
    step *= 2;
  }

  clock_gettime(CLOCK_REALTIME, &end);
  elapsed = end.tv_sec - begin.tv_sec;
  elapsed += (end.tv_nsec - begin.tv_nsec)/1000000000.0;
  FILE* stat = fopen("stats.txt", "a");

  fprintf(stat, "%d %d %d %lf qsort:%lf\n", n,init_m,P, elapsed, qsrt_elapsed);


  for (int i = 0; i < n; ++i) {
      fprintf(dat, "%d ", array[i]);
  }
  fclose(stat);
  fclose(dat);
  free(array);
  free(buf);
  free(edges);
  return 0;
}
