#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>
#include<time.h>

float pl, pr, pu, pd;
int l,a,b,N, n;
struct timespec begin_time, end_time;
double elapsed;

int calc_m(int size, int myrank) {
  int m;
  if ((int)b%size == 0) {
    m = b/size;
  } else {
    if (myrank < b%size) {
      m = b/size + 1;
    } else {
      m = b/size;
    }
  }
  return m;
}

int calc_start(int size, int rank) {
  int k;
  if (b%size == 0) {
    k = rank*b/size;
  } else {
    int p = b%size;
    if (p < rank) {
      k = (b/size + 1)*p + (rank - p)*(b/size);
    } else {
      k = (b/size + 1)*rank;
    }
  }
}

int main(int argc, char* argv[]) {
  if (argc > 9) {
    l = atoi(argv[1]);
    a = atoi(argv[2]);
    b = atoi(argv[3]);
    n = atoi(argv[4]);
    N = atoi(argv[5]);
    pl = atof(argv[6]);
    pr = atof(argv[7]);
    pu = atof(argv[8]);
    pd = atof(argv[9]);
  }
  MPI_Status Status;
  MPI_File fh;
  int myrank, size;
  int i, j, p, m, move;
  n = a*l*l*a*b;
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_File_open(MPI_COMM_WORLD, "data.bin",
   MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
  clock_gettime(CLOCK_REALTIME, &begin_time);
  srand(begin_time.tv_sec/(myrank+1));
  int** array = (int**) malloc(b*sizeof(int*));
  for (i = 0; i < b; ++i) {
    array[i] = (int*) malloc(a*sizeof(int));
    for (j = 0; j < a;++j)
      array[i][j] = 0;
  }
  m = calc_m(size, myrank);
  int start = calc_start(size, myrank);
  //printf("me:%d from %d to %d", myrank, start, start+m);
  for (i = start; i < start+m;++i){
    for (j = 0; j < a; ++j) {
      for (p = 0; p < N; ++p) {
        int x = j*l + rand() % l;
        int y = i*l + rand() % l;
        for (move = 0; move < n; ++move) {
          int r = rand() % 1000;
          if (r < pl*1000) {
            x--;
          } else if (r < (pl+pr)*1000) {
            x++;
          } else if (r < (pl+pr+pu)*1000) {
            y--;
          } else {
            y++;
          }
          if (x < 0) x = a*l + x;
          if (y < 0) y = b*l + y;
        }
        x = (x%(a*l))/l;
        y = (y%(b*l))/l;
        array[y][x]++;
      }
    }
  }
  int st;
  int** result = (int**) malloc(b*sizeof(int*));
  for (i = 0; i < b; ++i){
    if (myrank == 0)
      result[i] = (int*) malloc(a*sizeof(int));
    st = MPI_Reduce(array[i], result[i], a, MPI_INT, MPI_SUM,0,MPI_COMM_WORLD);
    if (st != MPI_SUCCESS) {
      printf("reduce failure");
    }
  }

  MPI_Barrier(MPI_COMM_WORLD);
  if (myrank == 0) {
    clock_gettime(CLOCK_REALTIME, &end_time);
    elapsed = end_time.tv_sec - begin_time.tv_sec;
    elapsed += (end_time.tv_nsec - begin_time.tv_nsec)/1000000000.0;
    FILE* filedesc = fopen("stats.txt", "w");
    fprintf(filedesc, "%d %d %d %d %d %f %f %f %f %lf\n", l,a,b,n,N, pl,pr,pu,pd, elapsed);
    for (i = 0; i < b; ++i) {
      for (j = 0; j < a; ++j) {
        fprintf(filedesc, "%d: %d\n", i*b + j, result[i][j]);
      }
      free(result[i]);
    }
    fclose(filedesc);
  }
  for (i = 0; i < b; ++i) {
    free(array[i]);
  }
  free(result);
  free(array);
  MPI_File_close(&fh);
  MPI_Finalize();
  return 0;
}
