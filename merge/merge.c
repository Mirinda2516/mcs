#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "omp.h"


void generate(int* arr, int n) 
{
	int i;
	for(i = 0; i < n; i++) {
		arr[i] = rand() % n;
	}
}

void printList(int* x, int n) {
	int i;
	for (i = 0; i < n; i++) {
		printf("%d ",x[i]);
	}
	printf("\n");
}

void merge(int* X, int n) 
{
	int* tmp = (int*) malloc(sizeof(int)*n);
	int i = 0;
	int j = n/2;
	int ti = 0;

	while (i < n / 2 && j < n) {
		if (X[i] < X[j]) {
			tmp[ti] = X[i];
			ti++; 
			i++;
		} else {
			tmp[ti] = X[j];
			ti++; j++;
		}
	}
	while (i < n / 2) {
		tmp[ti] = X[i];
		ti++; 
		i++;
	}
	while (j < n) {
		tmp[ti] = X[j];
		ti++; 
		j++;
	}
	memcpy(X, tmp, n*sizeof(int));
	free(tmp);
}

void bubble(int* X, int n)
{
	int i,j;
	for(i = 0; i < n; i++) {
		for(j = 0; j < n - 1; j++) {
			if(X[j] > X[j + 1]) {
				int tmp = X[j + 1];
				X[j + 1] = X[j];
				X[j] = tmp;
			}
		}
	}
	
}  

void mergesort(int* X, int n, int chunkSize)
{
	if (n <= chunkSize) {
		bubble(X,n);
		return;	
	}	

	#pragma omp task firstprivate (X, n)
	mergesort(X, n/2, chunkSize);

	#pragma omp task firstprivate (X, n)
	mergesort(X+(n/2), n-(n/2), chunkSize);
 
	#pragma omp taskwait

	merge(X, n);
}

int cmp(const void* a, const void* b)
{
	return *(int*)a - *(int*)b;
}

void quick(int* X, int n)
{
	qsort(X,n,sizeof(int),cmp);
}

int main(int argc, char* argv[])
{
	srand((unsigned int)time(NULL));
	int n = atoi(argv[1]);
	int m = atoi(argv[2]);
	int P = atoi(argv[3]);
	
	


	omp_set_num_threads(P);
	double start, stop;

	int* data = (int*)malloc(n * sizeof(int));
	int* qdata = (int*)malloc(n * sizeof(int));
  	int* oldData = (int*)malloc(n * sizeof(int));
	
	generate(data,n);
   	memcpy(oldData, data, n * sizeof(int));
	memcpy(qdata, data, n * sizeof(int));
	
	start = omp_get_wtime();
	#pragma omp single
	quick(qdata, n);	

	stop = omp_get_wtime();
	//printf("time : %f \n", stop - start);
	
	start = omp_get_wtime();
	#pragma omp parallel 
	{
		#pragma omp single
		mergesort(data, n, m);
	}
	stop = omp_get_wtime();
  
	
	freopen("stats.txt","wt",stdout);	
	printf("%fs %d %d %d\n",stop-start,n,m,P);
	printList(oldData, n);
	printList(data, n);
	free(data);
}


