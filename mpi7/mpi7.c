#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>
#include<time.h>

int a = 4;
int b = 4;
int N = 100;
int l = 2;
struct timespec begin_time, end_time;
double elapsed;

int calc_m(int size, int myrank) {
  int m;
  if ((int)b%size == 0) {
    m = b/size;
  } else {
    if (myrank < b%size) {
      m = b/size + 1;
    } else {
      m = b/size;
    }
  }
  return m;
}

int calc_start(int size, int rank) {
  int k;
  if (b%size == 0) {
    k = rank*b/size;
  } else {
    int p = b%size;
    if (p < rank) {
      k = (b/size + 1)*p + (rank - p)*(b/size);
    } else {
      k = (b/size + 1)*rank;
    }
  }
}

struct Point {
  int x, y, val;
};

int cmp(const void* a, const void* b) {
  struct Point left = *(struct Point*) a;
  struct Point right = *(struct Point*) b;
  if (left.y == right.y) {
    return left.x - right.y;
  } else {
    return left.y - right.y;
  }
}

int main(int argc, char* argv[]) {
  if (argc > 4) {
    l = atoi(argv[1]);
    a = atoi(argv[2]);
    b = atoi(argv[3]);
    N = atoi(argv[4]);
  }
  MPI_Status Status;
  MPI_File fh;
  int myrank, size;
  int i, j, p, m, n;
  n = a*l*l*a*b;
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_File_open(MPI_COMM_WORLD, "data.bin",
   MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);

  MPI_Barrier(MPI_COMM_WORLD);
  if (myrank == 0) {
    clock_gettime(CLOCK_REALTIME, &begin_time);
  }
  m = calc_m(size, myrank);
  int start = calc_start(size, myrank);
  struct Point* array = (struct Point*) malloc(sizeof(struct Point)*a*N*m);
  for (i = start; i < start+m;++i){
    for (j = 0; j < a; ++j) {
      for (p = 0; p < N; ++p) {
        array[(i - start)*a*N + j*N + p].x = j*l + rand() % l;
        array[(i - start)*a*N + j*N + p].y = (i - start)*l + rand() % l;
        array[(i - start)*a*N + j*N + p].val = rand() % (a*b);
      }
    }
  }
  qsort(array, a*N*m, sizeof(struct Point), cmp);
  int* buf = (int*) malloc(n*m*sizeof(int));
  for (j =0; j < n*m; ++j){
    buf[j] = 0;
  }
  for (j = 0; j < a*N*m; ++j) {
    buf[array[j].y * (n/l) + array[j].x *a*b + array[j].val]++;
  }
  MPI_File_write_at(fh, start*n*sizeof(int), buf, n*m, MPI_INT, &Status);
  //251649qw
  /*for (int j = 0; j < a*N; ++j) {
    printf("point %d %d rank %d\n", array[j].x, array[j].y, myrank);
  }*/
  free(buf);
  free(array);

  MPI_Barrier(MPI_COMM_WORLD);
  if (myrank == 0) {
    clock_gettime(CLOCK_REALTIME, &end_time);
    elapsed = end_time.tv_sec - begin_time.tv_sec;
    elapsed += (end_time.tv_nsec - begin_time.tv_nsec)/1000000000.0;
    FILE* filedesc = fopen("stats.txt", "w");
    fprintf(filedesc, "%d %d %d %d %lf\n", l,a,b,N, elapsed);
    fclose(filedesc);
  }

  MPI_File_close(&fh);
  MPI_Finalize();
  return 0;
}
